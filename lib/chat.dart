
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tely/drawer.dart';

import 'chat_details.dart';

class ChatsScreen extends StatefulWidget {
  @override
  _ChatsScreenState createState() => _ChatsScreenState();
}

class _ChatsScreenState extends State<ChatsScreen> {
  List<Map<String, dynamic>> _chats = [
    {
      'chat': {
        'chat_id': 1,
        'last_msg': 'لا☺☺ تدفع كاام؟؟',
        'un_read_msg_count': 1,
        'time': '2:20'
      },
      'sender': {
        'sender_id': 1,
        'sender_name': 'Mahmoud R Hamza',
        'sender_img': "assets/MRH.jpg",

      },
      'reciever': {
        'reciever_id': 1,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'last seen recently'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'عامل اي  ',
        'un_read_msg_count': 2,
        'time': '1:20'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'Sayed',
        'sender_img':
        "assets/user.png",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'last seen recently'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'ع اتفاقنا بقي  ',
        'un_read_msg_count': 5,
        'time': '4:00'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'Magdyeee',
        'sender_img':
        "assets/magdy.jpg",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'online'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'Ok  ',
        'un_read_msg_count': 2,
        'time': '3:10'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'Ezz',
        'sender_img':
        "assets/user.png",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'online'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'اي حاجه بقي  ',
        'un_read_msg_count': 10,
        'time': '7:25'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'اخوياا',
        'sender_img':
        "assets/bro.jpg",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'last seen recently'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'tmam  ',
        'un_read_msg_count': 3,
        'time': '8:17'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'ِAbodaa',
        'sender_img':
        "assets/aboda.jpg",
        'online':'last seen recently'
      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
    'online':'last seen recently'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'ف الفرح ',
        'un_read_msg_count': 1,
        'time': '5:30'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'Shahbour',
        'sender_img':
        "assets/shahbour.jpg",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
    'online':'last seen recently'
      }
    },
    {
      'chat': {
        'chat_id': 2,
        'last_msg': 'يوم السبت بقي',
        'un_read_msg_count': 4,
        'time': '9:10'
      },
      'sender': {
        'sender_id': 2,
        'sender_name': 'Samia',
        'sender_img':
        "assets/user.png",

      },
      'reciever': {
        'reciever_id': 2,
        'reciever_name': 'Sayed ',
        'sender_img':
        "assets/user.png",
        'online':'last seen recently'
      }
    }
];
  Widget _chatCard({int i}) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatDetailsScreen(
                  senderName: _chats[i]['sender']['sender_name'],
                  senderImage: _chats[i]['sender']['sender_img'],
                  online:_chats[i]['reciever']['online'],

                )));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: 100,
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: [
            Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image:
                      AssetImage(_chats[i]['sender']['sender_img']),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(80),
                  color: Color(0xff507ca1)),
            ),
            SizedBox(
              width: 10,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _chats[i]['sender']['sender_name'],
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(_chats[i]['chat']['last_msg'],style: TextStyle(color: Colors.grey[600],fontWeight: FontWeight.bold),)
              ],
            ),
            Expanded(child: SizedBox()),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(_chats[i]['chat']['time']),
                SizedBox(
                  height: 6,
                ),
                Container(
                  width: 25,
                  height: 25,
                  alignment: Alignment.topLeft,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.green),
                  child: Center(
                    child: Text(
                      _chats[i]['chat']['un_read_msg_count'].toString(),
                      style: TextStyle(color: Colors.white, fontSize: 13),overflow: TextOverflow.ellipsis,maxLines: 2,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xff507ca1),
          title: Text(
            'Telegram',
            style: TextStyle(
                color: Colors.white, fontSize: 20),
          ),
          leading: InkWell(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => DrawerScreen(),));
            },
              child: Icon(Icons.menu,color: Colors.white,)),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Icon(Icons.search,color: Colors.white,),
            )
          ],
        ),
        body: ListView.builder(
            itemCount: _chats.length,
            itemBuilder: (context,i) => _chatCard(i: i),
            ),
      ),
    );
  }
}
