import 'package:flutter/material.dart';
import 'appbar.dart';
import 'chat_details.dart';

class ChatDetailsScreen extends StatefulWidget {
  final String senderName;
  final String senderImage;
  final String online;

  ChatDetailsScreen({this.senderName, this.senderImage,this.online});

  @override
  _ChatDetailsScreenState createState() => _ChatDetailsScreenState();
}

class _ChatDetailsScreenState extends State<ChatDetailsScreen> {

  IconData icon = Icons.mic_none;

  //message
  List<Map<String, dynamic>> _message = [

    {
    'sender':{
      'sender_id': 1,
      'sender_msg':'السلام عليكم ورحمه الله وبركاته',
      'sender_name': 'Sayed',
      'time': '12:00'
    },

  },
    {
      'sender':{
        'sender_id': 1,
        'sender_msg':'وعليكم السلام ورحمه الله وبركاته',
        'sender_name': 'Sayed',
        'time': '12:01'
      },
    },
    {
      'sender':{
        'sender_id': 1,
        'sender_msg':'How are U my broo♥♥?',
        'sender_name': 'Sayed',
        'time': '12:05'
      },

    },
    {
      'sender':{
        'sender_id': 1,
        'sender_msg':'I am fine☻☻, what about u ?',
        'sender_name': 'Sayed',
        'time': '12:12'
      },
    },
    {
      'sender':{
        'sender_id': 1,
        'sender_msg':'ابعت ياعم اخر صور كدا اتصورناها  ',
        'sender_name': 'Sayed',
        'time': '12:15'
      },


    },
  ];

  //Bottom Sheet
  List<Map<String, dynamic>> _anotherMsg = [
    {
      'isSelected': true,
      'title': 'Gallery',
      'icon': Icons.image,
      'color': Colors.indigo,
    },
    {
      'isSelected': false,
      'title': 'File',
      'icon': Icons.insert_drive_file,
      'color': Colors.blue,
    },
    {
      'isSelected': false,
      'title': 'Location',
      'icon': Icons.location_on,
      'color': Colors.green,
    },
    {
      'isSelected': false,
      'title': 'Contact',
      'icon': Icons.person,
      'color': Colors.orangeAccent,
    },
    {
      'isSelected': false,
      'title': 'Video',
      'icon': Icons.video_call_sharp,
      'color': Colors.red,
    },
  ];
  int selectedItem = 0;
  _buttonSheet() {
    return showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                Expanded(
                  child: GridView.count(
                    crossAxisCount: 4,
                    mainAxisSpacing: 2,
                    crossAxisSpacing: 2,
                    children: List.generate(
                      23,
                      (index) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Image(
                            image: AssetImage('assets/chats/gallary.jpg'),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                Container(
                  height: 90,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: 5,
                    itemBuilder: (context, i) => _item(
                        index: i,
                        isSelected: _anotherMsg[i]['isSelected'],
                        title: _anotherMsg[i]['title'],
                        icon: _anotherMsg[i]['icon'],
                        color: _anotherMsg[i]['color']),
                  ),
                ),
              ],
            ),
          );
        });
  }

  _item({String title, IconData icon, Color color, int index, bool isSelected}) {
    return InkWell(
      onTap: () {
        setState(() {
          _anotherMsg[selectedItem]['isSelected'] = true;
          selectedItem = index;
          _anotherMsg[selectedItem]['isSelected '] = true;
        });
      },
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50), color: color),
              child: Icon(
                icon,
                color: Colors.white,
              ),
            ),
          ),
          Text(
            title,
            style: TextStyle(color: isSelected ? color : Colors.grey),
          ),
        ],
      ),
    );
  }

  Widget _sendMessageArea() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8),
      height: 70,
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Icon(
            Icons.tag_faces,
            size: 25,
            color: Colors.grey,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration.collapsed(
                hintText: 'Messages..',
              ),
              textCapitalization: TextCapitalization.sentences,
            ),
          ),
          InkWell(
            onTap: () {
              _buttonSheet();
            },
            child: Icon(
              Icons.attach_file,
              size: 25,
              color: Colors.grey,
            ),
          ),
          SizedBox(width: 5,),
          InkWell(
             onTap: () {
               setState(() {
                 icon = Icons.video_call ;
               });
             },
            child: Icon(
             icon,
              size: 25,
              color: Colors.grey,
            ),
          ),SizedBox(width: 8,),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(
          context: context,
          senderImage: widget.senderImage,
          senderName: widget.senderName,
           online:widget.online),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  'assets/background.png',
                ),
                fit: BoxFit.fill)),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                itemCount: _message.length,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: index % 2 == 0
                        ? MainAxisAlignment.end
                        : MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.all(5),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          color:
                              index % 2 == 0 ? Colors.blue[300] : Colors.white,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: index % 2 == 0
                            ? Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 0),
                                  child: Text(_message[index]['sender'][ 'sender_msg'],overflow: TextOverflow.ellipsis,maxLines: 2),
                                ),
                                Row(
                                  children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 110),
                                    child: Text(_message[index]['sender']['time'],style: TextStyle(color: Colors.blue[900],fontSize: 13),),
                                  ),
                                  Icon(Icons.check,color: Colors.blue[900],size: 20,)
                                ],),
                              ],
                            )
                            :  Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 0),
                              child: Text(_message[index]['sender'][ 'sender_msg'],overflow: TextOverflow.ellipsis,maxLines: 2),
                            ),
                            Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 110),
                                  child: Text(_message[index]['sender']['time'],style: TextStyle(color: Colors.blue[900],fontSize: 13),),
                                ),
                                Icon(Icons.check,color: Colors.blue[900],size: 20,)
                              ],),
                          ],
                        )
                      ),

                    ],
                  );
                },
              ),
            ),
            _sendMessageArea(),
          ],
        ),
      ),
    );
  }
}
