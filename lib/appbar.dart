import 'package:flutter/material.dart';

Widget appBar({BuildContext context, String senderName, String senderImage,String online}) {
  return AppBar(
    backgroundColor: Color(0xff507ca1),
    automaticallyImplyLeading: false,
    leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.of(context).pop();
        }),
    title: Row(
      children: [
        Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(40),
              color: Colors.white,
              image: DecorationImage(
                  image: AssetImage(senderImage), fit: BoxFit.cover)),
        ),
        SizedBox(
          width: 6,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(senderName),
            Text(online,style: TextStyle(color: Colors.grey,fontSize: 12),),
          ],
        )
      ],
    ),
    actions: [
      PopupMenuButton(
        itemBuilder: (context) {
          return list.map((e) {
            return PopupMenuItem(
              value: e,
                child:Row(
                  children: [
                    IconButton(icon:Icon(e.icon) ),
                    Text(e.title)
                  ],
                )
            );
          }).toList();
        },
      ),
    ],
  );
}
class PopupItem{
  String title;
  IconData icon;
  PopupItem({this.title, this.icon});
}
var list = <PopupItem>[
  PopupItem(title:'Call',icon:Icons.call),
  PopupItem(title:'Video Call',icon:Icons.video_call_sharp),
  PopupItem(title:'Search',icon:Icons.search),
  PopupItem(title:'Share my contact',icon:Icons.person_add),
  PopupItem(title:'Clear history',icon:Icons.clear),
  PopupItem(title:'Mute notification',icon:Icons.volume_off_outlined),
  PopupItem(title:'Delete chat',icon:Icons.delete_outline),
];