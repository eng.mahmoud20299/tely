import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/new_channel.dart';
import 'package:tely/drawerScreens/settings.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawerScreenState createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  Widget _profileContainer({double px, String img}) {
    return Container(
      width: px,
      height: px,
      child: CircleAvatar(
        backgroundImage: AssetImage(img),
      ),
    );
  }

  Widget _buildText({String text, Color color, double fontSize}) {
    return Text(
      text,
      style: TextStyle(color: color, fontSize: fontSize),
    );
  }

  Widget _listTile({IconData icon, String text,Widget screen}) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
        Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => screen));
      },
      child: ListTile(
        leading: Icon(icon, color: Colors.grey),
        title: Text(text,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(color: Color(0xff507ca1)),
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        _profileContainer(px: 60, img: 'assets/user.png'),
                        IconButton(icon:Icon (Icons.brightness_2,color: Colors.white,),)
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            _buildText(
                                text: 'Mahmoud R Hamza',
                                color: Colors.white,
                                fontSize: 14),
                            _buildText(
                                text: '+010********',
                                color: Colors.white,
                                fontSize: 14),
                          ],
                        ),
                        IconButton(icon:Icon (Icons.keyboard_arrow_down,size: 35,color: Colors.white),),
                      ],
                    ),
                  ],
                ),
              ),
              _listTile(icon: Icons.people_outline, text: 'مجموعة جديدة'),
              _listTile(icon: Icons.lock_outline, text: 'محادثة سرية جديدة'),
              _listTile(icon: Icons.show_chart, text: 'New Channel',screen:NewChannel() ),
              _listTile(icon: Icons.person_outline, text: 'جهات الاتصال'),
              _listTile(icon: Icons.call, text: 'المكالمات'),
              _listTile(icon: Icons.bookmark_border, text: ' الرسائل المحفوظة'),
              _listTile(icon: Icons.settings, text: 'Settings',screen: SettingsScreen()),
              Divider(height: 2,color: Colors.grey,),
              _listTile(icon: Icons.person_add, text: ' دعوة أصدقاء'),
              _listTile(icon: Icons.help_outline, text: 'الأسئلة الشائعة'),
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Color(0xff507ca1),
        ),
      ),
    );
  }
}
