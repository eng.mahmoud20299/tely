import 'package:flutter/material.dart';
import 'package:tely/chat.dart';
import 'package:tely/drawer.dart';
import 'package:tely/drawerScreens/settings.dart';
import 'package:tely/drawerScreens/settings_screen/stickers_masks.dart';
import 'package:tely/drawerScreens/settings_screen/stickers_pages/masks.dart';

import 'drawerScreens/settings_screen/privacy.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home:ChatsScreen(),
    );
  }
}
