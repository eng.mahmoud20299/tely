import 'package:flutter/material.dart';
import 'package:tely/drawer.dart';
class NewChannel extends StatefulWidget {
  @override
  _NewChannelState createState() => _NewChannelState();
}
class _NewChannelState extends State<NewChannel> {
  Widget _buildField({String hint,double width,dynamic margin}){
    return Container(
      margin: margin,
      width: width,
      child: TextFormField(
        decoration: InputDecoration(
            hintText:hint,
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context) => DrawerScreen(),));
          },
            child: Icon(Icons.arrow_back,color: Colors.white,)),
        title: Text('New Channel'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Icon(Icons.done,color:Colors.white),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15),
          child: Row(
            children: <Widget>[
          Container(
          width:80,
          height: 80,
          child: CircleAvatar(
            backgroundColor: Color(0xff507ca1),
            child: Icon(Icons.photo_camera,color: Colors.white,),
          ),
      ),
              _buildField(hint: 'Channel name',margin: EdgeInsets.only(left: 15),width: MediaQuery.of(context).size.width-135,),

            ],
          ),
        ),
          _buildField(hint: 'Description',width: MediaQuery.of(context).size.width-40,),
          SizedBox(height: 10,),
          Container(
            padding: EdgeInsets.only(left: 15),
            margin: EdgeInsets.only(right: 10),
            child: Column(
              crossAxisAlignment:CrossAxisAlignment.end,
              children: <Widget>[
              Text('You can provide an optional description for your channel.'),
            ],),
          )
      ],
      ),
    );
  }
}
