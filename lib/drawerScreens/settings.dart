import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tely/drawer.dart';
import 'package:tely/drawerScreens/settings_screen/bio.dart';
import 'package:tely/drawerScreens/settings_screen/change_number.dart';
import 'package:tely/drawerScreens/settings_screen/privacy.dart';
import 'package:tely/drawerScreens/settings_screen/user_name.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'package:url_launcher/url_launcher.dart';
class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}
enum AppState {
  free,
  picked,
  cropped,
}

class _SettingsScreenState extends State<SettingsScreen> {
  AppState state;
  File imageFile;

  @override
  void initState() {
    super.initState();
    state = AppState.free;
  }

  Future<Null> _pickImage() async {
    imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (imageFile != null) {
      setState(() {
        state = AppState.picked;
      });
    }
  }

  Future<Null> _cropImage() async {
    File croppedFile = await ImageCropper.cropImage(

        sourcePath: imageFile.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ]
            : [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio5x3,
          CropAspectRatioPreset.ratio5x4,
          CropAspectRatioPreset.ratio7x5,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.blue,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      imageFile = croppedFile;
      setState(() {
        state = AppState.cropped;
      });
    }
  }

  void _clearImage() {
    imageFile = null;
    setState(() {
      state = AppState.free;
    });
  }

  Widget _buildButtonIcon() {
    if (state == AppState.free)
      return Icon(Icons.camera_alt,color: Colors.grey,);
    else if (state == AppState.picked)
      return Icon(Icons.crop,color: Colors.grey,);
    else if (state == AppState.cropped)
      return Icon(Icons.clear,color: Colors.grey,);
    else
      return Container();
  }
  Widget _buildListTile({String title,IconData icon,}){
    return ListTile(
      leading:Icon(icon),
      title: Text(title),
    );
  }
  Widget _text({String title,Color color,FontWeight weight,double size}){
    return  Text(title,style: TextStyle(color: color,fontWeight: weight,fontSize: size),);
}
Widget _buildInkwell({String text1,String text2, Widget screen}){
    return  InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (context) =>screen,));
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _text(title:text1,color: Colors.black,weight: FontWeight.bold ), SizedBox(height: 5,) ,
          _text(title:text2,color: Colors.grey,weight: FontWeight.bold ),
        ],),
    );
  }
  Widget _divider({double height,double indent,double thickness,Color color }){
    return Divider(
      height: height,
      indent:indent ,
      thickness:thickness ,
      color: color,
    );
  }
   launchURL({String link}) async {
    var url = link;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height-430,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 140,
                color: Color(0xff507ca1),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => DrawerScreen(),));
                            },
                              child: Icon(Icons.arrow_back,color: Colors.white,)),SizedBox(width: 240,),
                          Icon(Icons.search,color: Colors.white,),SizedBox(width:20,),
                          Icon(Icons.more_vert,color: Colors.white,)
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(15),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width:60,
                            height: 60,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(60),
                            ),
                            child:imageFile != null ? Image.file(imageFile,fit: BoxFit.fill,) : Image(image: AssetImage('assets/user.png'),),
                            ),

                          SizedBox(width:20,),
                          _text(title:'Mahmoud R Hamza',color: Colors.white,weight: FontWeight.bold,size: 20  ),
                        ],),
                    )
                  ],
                ),
              ),
              Positioned(
                top: 110,
                left: MediaQuery.of(context).size.width-80,
                child: FloatingActionButton(
                  backgroundColor: Colors.white,
                    child: _buildButtonIcon(),
                    onPressed:(){
                      if (state == AppState.free)
                        _pickImage();
                      else if (state == AppState.picked)
                        _cropImage();
                      else if (state == AppState.cropped) _clearImage();
                    }
                    )
              ),
          Positioned(
            top: 160,
            child: Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _text(title:'Account',color: Color(0xff507ca1),weight:FontWeight.bold), SizedBox(height: 20,) ,
                    _buildInkwell(text1:'+20 **********', text2:'Tap to change phone number',screen: ChangeNumberScreen(),),
                    _divider(height: 3,indent: 50,),
                    SizedBox(height: 5,) ,
                    SizedBox(height: 5,) ,
                    _buildInkwell(text1: 'None',text2:'Username',screen: UserNameScreen(), ),
                     SizedBox(height: 5,) ,
                    _divider(height: 3,indent: 50),
                    SizedBox(height: 5,) ,
                    _buildInkwell(text1: 'Bio',text2:'Add a few words about yourself',screen: BioScreen(),  ),
                    SizedBox(height: 5,) ,
                ],
                ),
              ),
            ),
          ),
            ],
          ),
          Divider(
            color: Colors.grey[300],
            height: 4,
            thickness: 15,
          ),
          Padding(
           padding: const EdgeInsets.only(left: 15,top: 10),
            child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _text(title: 'Settings',color: Color(0xff507ca1),weight: FontWeight.bold),
                    _buildListTile(icon: Icons.notifications_none,title: 'Notifications and Sounds'),
                    _divider(height: 1,indent: 70),
                    InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>Privacy()));
                        },
                        child: _buildListTile(icon: Icons.lock_outline,title: 'Privacy and Security')),
                    _divider(height: 1,indent: 70),
                    _buildListTile(icon: Icons.timelapse,title: 'Data and Storage'),
                    _divider(height: 1,indent: 70),
                    _buildListTile(icon: Icons.chat_bubble_outline,title: 'Chat Settings'),
                    _divider(height: 1,indent: 70),
                    _buildListTile(icon: Icons.folder_open,title: 'Folders'),
                    _divider(height: 1,indent: 70),
                    _buildListTile(icon: Icons.laptop,title: 'Devices'),
                    _divider(height: 1,indent: 70),
                    _buildListTile(icon: Icons.language,title: 'Language'),
                  ],
                )),
          ),
          _divider(color: Colors.grey[300], height: 4, thickness: 15,),
          Padding(
            padding: const EdgeInsets.only(left: 15,top: 10),
            child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _text(title:'Help',color: Color(0xff507ca1),weight: FontWeight.bold ),
                    _buildListTile(icon: Icons.sms,title: 'Ask a Question'),
                    _divider(height: 1,indent: 70),
                    InkWell(
                      onTap: (){
                        launchURL(link: 'https://telegram.org/faq');
                      },
                        child: _buildListTile(icon: Icons.help_outline,title: 'Telegram FAQ')),
                    _divider(height: 1,indent: 70),
                    InkWell(
                        onTap: (){
                          launchURL(link: 'https://telegram.org/privacy ');
                        },
                        child: _buildListTile(icon: Icons.verified_user,title: 'Privacy Policy')),
                  ],
                )),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            color: Colors.grey[300],
            child: Center(child: _text(title:'Telegram for Android v7.0.0(2064) arm64-v8a',color: Colors.grey ),),
          ),
        ],
      ),
    );
  }
}
