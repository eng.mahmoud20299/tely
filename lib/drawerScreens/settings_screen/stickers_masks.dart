import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings_screen/stickers_pages/masks.dart';

class StickersAndMasksScreen extends StatefulWidget {
  @override
  _StickersAndMasksScreenState createState() => _StickersAndMasksScreenState();
}

class _StickersAndMasksScreenState extends State<StickersAndMasksScreen> {
  bool value = false;
  int switchValue;

  Widget _img({String img}) {
    return Container(
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 65,
      width: 65,
      child: Image(
        image: AssetImage(img),
      ),
    );
  }

  Widget _buildRow({
    String text1,
    String text2,
  }) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(text1),
          Text(
            text2,
            style: TextStyle(color: Colors.blue),
          ),
        ],
      ),
    );
  }

  Widget _buildLine() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
      ),
      height: 1,
      decoration: BoxDecoration(color: Colors.black12),
    );
  }

  Widget _bottomSheet() {
    return Column(
      children: <Widget>[
        ListTile(
          title: Text('Buddy Bear'),
          subtitle: Text('25 stickers'),
          trailing: FlatButton(
            child: Container(
                height: 25,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.blue,
                ),
                child: Center(
                    child: Text(
                  'Add',
                  style: TextStyle(color: Colors.white),
                ))),
          ),
        ),
        Row(
          children: <Widget>[
            _img(img: 'assets/maingif2.png'),
            _img(img: 'assets/maingif2.png'),
            _img(img: 'assets/maingif2.png'),
            _img(img: 'assets/maingif2.png'),
            _img(img: 'assets/maingif2.png'),
          ],
        ),
        Divider(),
      ],
    );
  }

  Widget _radioList({int value, String text}) {
    return RadioListTile(
      value: value,
      groupValue: switchValue,
      onChanged: (newValue) {
        setState(() {
          switchValue = newValue;
        });
        Navigator.pop(context);
      },
      title: Text(text),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            )),
        title: Text('Stickers and Masks'),
      ),
      body: ListView(
        children: <Widget>[
          InkWell(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Suggest stickers by emoji'),
                        content: Container(
                          height: 170,
                          child: Column(
                            children: <Widget>[
                              _radioList(value: 1, text: 'All Sets'),
                              _radioList(value: 2, text: 'My Sets'),
                              _radioList(value: 3, text: 'None'),
                            ],
                          ),
                        ),
                      );
                    });
              },
              child: _buildRow(
                  text1: 'Suggest stickers by emoji', text2: 'All Sets')),
          SizedBox(
            height: 20,
          ),
          _buildLine(),
          Padding(
            padding: const EdgeInsets.only(
              left: 15,
              right: 15,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Loop Animated Stickers'),
                Switch(
                  value: value,
                  onChanged: (newValue) {
                    setState(() {
                      value = newValue;
                    });
                  },
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            color: Colors.grey[200],
            child: Padding(
              padding: const EdgeInsets.only(left: 15, top: 10),
              child: Text(
                'Animated stickers will play continuously in chats.',
                style: TextStyle(
                  color: Colors.grey[500],
                ),
              ),
            ),
          ),
          InkWell(
              onTap: () {
                showModalBottomSheet(
                    context: context,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                    builder: (BuildContext context) {
                      return Container(
                        height: MediaQuery.of(context).size.height,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 20),
                          child: Column(
                            children: <Widget>[
                              SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.width / 3 - 80,
                                child: TextFormField(
                                  decoration: InputDecoration(
                                    hintText: 'Search trending stickers',
                                    prefixIcon: Icon(Icons.search),
                                    filled: true,
                                    fillColor: Colors.grey[200],
                                    contentPadding: EdgeInsets.symmetric(
                                        horizontal: 5, vertical: 5),
                                    border: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(30)),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Expanded(
                                child: ListView.builder(
                                  itemCount: 16,
                                  scrollDirection: Axis.vertical,
                                  itemBuilder: (context, index) {
                                    return _bottomSheet();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    });
              },
              child: _buildRow(text1: 'Trending Stickers', text2: '16')),
          SizedBox(
            height: 20,
          ),
          _buildLine(),
          InkWell(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MasksScreen(),
                    ));
              },
              child: _buildRow(text1: 'Masks', text2: '6')),
          SizedBox(
            height: 20,
          ),
          Container(
            height: MediaQuery.of(context).size.height - 332,
            color: Colors.grey[200],
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            'Artist are welcome to add their own sticker sets using our ',
                        style: TextStyle(color: Colors.grey[500])),
                    TextSpan(
                        text: '@stickers',
                        style: TextStyle(color: Colors.blue)),
                    TextSpan(
                        text: ' bot.',
                        style: TextStyle(color: Colors.grey[500])),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
