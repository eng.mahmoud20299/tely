import 'package:flutter/material.dart';

class MasksScreen extends StatefulWidget {
  @override
  _MasksScreenState createState() => _MasksScreenState();
}

class _MasksScreenState extends State<MasksScreen> {
  Widget _maskDetails(
      {String title, String subTitle, int count, String gif, String mainGif}) {
    return Padding(
      padding: const EdgeInsets.only(left: 15),
      child: InkWell(
        onTap: () {
          showModalBottomSheet(
              context: context,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              builder: (BuildContext context) {
                return Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: GridView.count(
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          crossAxisCount: 5,
                          scrollDirection: Axis.vertical,
                          children: List.generate(
                            count,
                            (index) {
                              return Image(
                                image: AssetImage(gif),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Center(
                          child: Text(
                        'REMOVE 20 MASKS',
                        style: TextStyle(color: Colors.red),
                      )),
                    ],
                  ),
                );
              });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              height: 40,
              width: 40,
              // color: Colors.blue,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage(mainGif))),
            ),
            Container(
              width: MediaQuery.of(context).size.width - 60,
              child: ListTile(
                title: Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Text(subTitle),
                trailing: InkWell(
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text(title),
                              content: Container(
                                height: MediaQuery.of(context).size.height /
                                        (4 * 2) +
                                    15,
                                child: Column(
                                  children: <Widget>[
                                    _listTile(
                                        text: 'Archive', icon: Icons.archive),
                                    _listTile(
                                        text: 'Reorder', icon: Icons.reorder),
                                  ],
                                ),
                              ),
                            );
                          });
                    },
                    child: Icon(Icons.more_vert)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _listTile({String text, IconData icon}) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: ListTile(
        leading: Icon(icon),
        title: Text(text),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        title: Text('Masks'),
      ),
      body: Column(
        children: <Widget>[
          _maskDetails(
              title: 'Cronavirus',
              subTitle: '32 stickers',
              count: 32,
              mainGif: 'assets/maingif1.png',
              gif: 'assets/crona.gif'),
          Divider(),
          _maskDetails(
              title: 'Masks |: Party Animals',
              subTitle: '40 stickers',
              count: 40,
              mainGif: 'assets/maingif2.png',
              gif: 'assets/partyanimals.gif'),
          Divider(),
          _maskDetails(
              title: 'Masks ||: Face Life',
              subTitle: '64 stickers',
              count: 64,
              mainGif: 'assets/maingif3.png',
              gif: 'assets/facelife.gif'),
          Divider(),
          _maskDetails(
              title: 'Masks |||: Costume Party',
              subTitle: '20 stickers',
              count: 20,
              mainGif: 'assets/maingif4.png',
              gif: 'assets/costumeparty.gif'),
          Divider(),
          _maskDetails(
              title: 'Masks |V: Masquerade',
              subTitle: '80 stickers',
              count: 80,
              mainGif: 'assets/maingif5.png',
              gif: 'assets/masquerade.gif'),
          Divider(),
          _maskDetails(
              title: 'Masks V: Glitter',
              subTitle: '85 stickers',
              count: 85,
              mainGif: 'assets/maingif6.png',
              gif: 'assets/glitter.gif'),
          Container(
            height: MediaQuery.of(context).size.height / 4 - 15,
            color: Colors.grey[300],
            child: Padding(
              padding: const EdgeInsets.only(left: 15, top: 5),
              child: Text(
                'You can add masks to photos you send. To do this, open the photo editor before sending photo',
                style: TextStyle(color: Colors.grey),
              ),
            ),
          )
        ],
      ),
    );
  }
}
