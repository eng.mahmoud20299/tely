import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/active_sessions.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/blocked_user.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/calls.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/forwarded_messages.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/groups.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/last_seen.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/logged_in_with_telegram.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/passcode_look.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/phone_number.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/profile_photo.dart';
import 'package:tely/drawerScreens/settings_screen/privacy_screens/two_step_verification.dart';

class Privacy extends StatefulWidget {
  @override
  _PrivacyState createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  bool switchval = false;
  void onswitchtochange(bool val) {
    setState(() {
      switchval = val;
    });
  }
  int selectedOne=0;
  int index;
  bool isSelected=true;
  Widget _buildtext({String text1}) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 20, top: 10,),
          child: Text(text1, style: TextStyle(color: Colors.blue, fontSize: 20,),),
        )
      ],
    );
  }
  Widget _buildgtext2({String text4}) {
    return Container(
      margin: EdgeInsets.only(left: 15, bottom: 5,),
      child: Text(text4, style: TextStyle(color: Colors.black38, fontSize: 15),),
    );
  }
  Widget _buildtext3({String text5}) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 20, top: 15, bottom: 15),
          child: Text(text5, style: TextStyle(fontSize: 17, color: Colors.black),),
        )
      ],
    );
  }
  Widget _buildrow1({String text2, String text3}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
          child: Text(
            text2,
            style: TextStyle(
              color: Colors.black,
              fontSize: 17,
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            child: Text(
              text3,
              style: TextStyle(
                color: Colors.blue,
                fontSize: 17,
              ),
            ))
      ],
    );
  }
  Widget _buildrow2({String text6}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: 20,
          ),
          child: Text(
            text6,
            style: TextStyle(color: Colors.black, fontSize: 17),
          ),
        ),
        Switch(
          value: switchval,
          onChanged: onswitchtochange,
        )
      ],
    );
  }
  Widget _buildline() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
      ),
      height: 1,
      decoration: BoxDecoration(color: Colors.black12),
    );
  }
  Widget _buildInkwell({String text1,String text2, Widget screen}){
    return InkWell(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => screen,));
        },
        child: _buildrow1(text2: text1, text3: text2));
  }
  Widget _buildRow({String title4,int index,bool isSelected}) {
    return
        InkWell(
        onTap: () {
          setState(() {
            isSelected = true;
            selectedOne = index;
            Navigator.of(context).pop();
          });
        },
          child: Row(
                children: <Widget>[
                  Radio(
                      value: index,
                      groupValue: selectedOne,
                      onChanged: (s){
                        setState(() {
                          isSelected  =true;
                          selectedOne = index;
                        });
                      }),
                  Text(title4, style: TextStyle(color: Colors.black, fontSize: 17,
                    ),
                  ),
                ],
              ),
        );

  }
  Widget _alert({String title1,String title2,String title3,String title4}){
    return InkWell(
      child:  _buildtext3(text5:title1),
      onTap: (){
        showDialog(context: context,
            builder: (BuildContext context){
              return AlertDialog(
                title: Text(title2),
                content: Text(title3),
                actions: <Widget>[
                  FlatButton(
                    child: Text( 'CANCEL',),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ),
                  FlatButton(
                    child: Text( title4,style:TextStyle(color: Colors.red)),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ),
                ],
              );
            }
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(240, 240, 240, 1),
      appBar: AppBar(
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
              Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen(),));
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text('Privacy and Security', style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            height: 430,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Privacy'),
                _buildInkwell(text1:'Blocked Users',text2:  'None',screen: BlockedUserScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Phone Number',text2:'My Contacts' ,screen: PhoneNumberScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Last Seen & Online',text2:  'Nobody',screen:  LastSeenScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Profile Photo',text2:  'Everybody',screen: ProfilePhotoScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Forwarded Messages',text2:  'Everybody',screen: ForwardedMessagesScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Calls',text2:  'Everybody',screen: CallsScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Groups',text2:  'Everybody',screen:  GroupsScreen(), ),
              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(text4: 'Change who can add you to groups and channels'),
          SizedBox(height: 10,),
          Container(
            height: 200,
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Security'),
                _buildInkwell(text1: 'Passcode Lock',text2: '',screen:   PasscodeLookScreen(), ),
                _buildline(),
                _buildInkwell(text1:'Two-Step Verification',text2: 'Off',screen:  TowStepVerificationScreen(), ),
                _buildline(),
                _buildInkwell(text1: 'Active Sessions',text2: '',screen:   ActiveSessionsScreen(), ),
              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(text4: 'Control your sessions on other devices'),
          SizedBox(height: 10,),
          Container(
            height: 90,
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Delete my account'),
                InkWell(onTap: (){
                  showDialog(context: context,
                      builder: (BuildContext context){
                        return AlertDialog(
                         title:Text('Account Self-destructs'),
                          content: Container(
                            height: 200,
                            width: 200,
                            child: Column(children: <Widget>[
                              _buildRow(title4: '1 month',index: 0,isSelected: false),
                              _buildRow(title4: '3 month',index: 1,isSelected: false),
                              _buildRow(title4: '6 month',index: 2,isSelected: true),
                              _buildRow(title4: '1 year',index: 3,isSelected: false),
                            ],
                             ),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text( 'CANCEL',),
                              onPressed: (){
                                Navigator.of(context).pop();
                              },
                              color: Colors.white,
                            ),
                          ],
                        );
                      }
                  );
                },
                    child: _buildrow1(text2: 'If away for', text3: '6 months'))
              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(
              text4: 'If  you do ot come online at least once within this'),
          _buildgtext2(
              text4: 'period, Your account will be deleted along with all'),
          _buildgtext2(text4: 'messages and contacts.'),
          SizedBox(height: 10,),
          Container(
            height: 140,
            decoration: BoxDecoration(color: Colors.white,),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Contacts'),
                InkWell(
                    onTap: (){
                      showDialog(context: context,
                          builder: (BuildContext context){
                            return AlertDialog(
                              title: Text('Clear payment info'),
                              content: Container(
                                height: 135,
                                child: Column(
                                  children: <Widget>[
                                    Text('Are you sure you want clear your payment and shipping info ? '),
                                    Row(
                                      children: <Widget>[
                                        Checkbox(
                                          onChanged: (s){
                                            setState(() {
                                              isSelected  =s;
                                              selectedOne = index;
                                            });
                                          },
                                          value:true,
                                        ),
                                        Text('Shipping info')
                                      ],
                                    ), Row(
                                      children: <Widget>[
                                        Checkbox(
                                            onChanged: (s){
                                              setState(() {
                                                isSelected  =s;
                                                selectedOne = index;
                                              });
                                            },
                                          value: true,
                                        ),
                                        Text('Payment info')
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text( 'CANCEL',),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                  color: Colors.white,
                                ),
                                FlatButton(
                                  child: Text( 'CLEAR',style:TextStyle(color: Colors.red)),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },
                                  color: Colors.white,
                                ),
                              ],

                            );
                          }
                      );
                    },
                    child: _buildtext3(text5: 'Clear Payment and Shipping Info')),
                _buildline(),
                _buildInkwell(text1: 'Logged in with Telegram',text2: '',screen: LoggedInwithTelegramScreen(), ),
              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(text4: 'Websites where you used Telegram to log in'),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 186,
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Contacts'),
                _alert(title1:'Delete Synced Contacts',title2: 'Delete contacts',title3: 'This will remove your contacts from the Telegram servers',title4: 'DELETE' ),
                _buildline(),
                _buildrow2(text6: 'Sync Contacts'),
                _buildline(),
                _alert(title1: 'Suggest Frequent Contacts',title2: 'Disable suggestions',title3: 'This will delete all data about the people you message frequently as well as the inline bots you are likely to use.',title4: 'DISABLE'),

              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(text4: 'Display people you message frequently ar the top'),
          _buildgtext2(text4: 'of the search section for quick access.'),
          SizedBox(height: 10,),
          Container(
            height: 140,
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: <Widget>[
                _buildtext(text1: 'Secret Chats'),
                InkWell(
                    onTap: (){
                      showDialog(context: context,
                          builder: (BuildContext context){
                            return AlertDialog(
                              title:Text('Map Preview Provider') ,
                              content: Container(
                                height: 150,
                                child: Column(
                                  children: <Widget>[
                                    _buildRow(title4: 'Telegram',index: 0,isSelected: false),
                                    _buildRow(title4: 'Google',index: 1,isSelected: false),
                                    _buildRow(title4: 'No previews',index: 2,isSelected: true),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text( 'CANCEL',),
                                  onPressed: (){
                                    Navigator.of(context).pop();
                                  },),
                              ],
                            );
                          });
                    },
                    child:_buildrow1(text2: 'Map Preview Provider', text3: 'No Previews'),),
                _buildline(),
                _buildrow2(text6: 'Link Preview')
              ],
            ),
          ),
          SizedBox(height: 10,),
          _buildgtext2(text4: 'Link preview will be generated on Telegram'),
          _buildgtext2(text4: 'srever.We do not store data about the links you'),
          _buildgtext2(text4: 'send.')
        ],
      ),
    );
  }
}
