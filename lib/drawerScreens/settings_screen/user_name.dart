import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings.dart';
class UserNameScreen extends StatefulWidget {
  @override
  _UserNameScreenState createState() => _UserNameScreenState();
}

class _UserNameScreenState extends State<UserNameScreen> {
  String text = '';
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,color: Colors.white,)),
        title: Text('Username'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: IconButton(icon: Icon(Icons.done,color:Colors.white),onPressed: (){ Navigator.pop(context);},),
          ),
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Container(
            width: MediaQuery.of(context).size.width-30,
            child: Form(
              key: formKey,
              child: TextFormField(
                validator: (text){
                  if(text.length<5  ){
                    return"A username must have at least 5 characters ";}
                  return null;
                },
                decoration: InputDecoration(
                  hintText:'Your Username',hintStyle:TextStyle(fontSize: 20),

              ),
                onChanged: (value){
                  setState(() {
                    if(formKey.currentState.validate()){
                      return null;
                    }
                  });
                  setState(() {
                    text=value;
                   return text.isEmpty ? null : text=('https://t.me/$value');
                  });
                },
              ),
            ),
          ),
              SizedBox(height: 15,),
              Text('You can choose a username on Telegram. if you do, other people will be able to find you by this username and contact you without knowing your phone number.',textScaleFactor: 1.3,style: TextStyle(color: Colors.grey[800],)),
              SizedBox(height: 20,),
              Text('You can use a-z, 0-9 and underscores. Minimum length is 5 characters.',textScaleFactor: 1.264,style: TextStyle(color: Colors.grey[800],)),
              SizedBox(height: 20,),
              Text('This link opens a chat with you:',textScaleFactor: 1.264,style: TextStyle(color: Colors.grey[800],),),
              Text(text,style: TextStyle(color: Colors.blue),),
            ],
          ),
        ),
      ),
    );
  }
}
