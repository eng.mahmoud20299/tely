import 'package:flutter/material.dart';

import '../settings.dart';

class ChangeNumberScreen extends StatefulWidget {
  @override
  _ChangeNumberScreenState createState() => _ChangeNumberScreenState();
}
class _ChangeNumberScreenState extends State<ChangeNumberScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only( top: 40,right: 340),
            child: Container(
                child: InkWell(
                  onTap: (){
                    Navigator.pop(context);
                  },
                  child: Icon(
              Icons.arrow_back,
              color: Colors.grey,
            ),
                )),
          ),
          SizedBox(height: 100,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.sim_card,size: 90,color: Colors.grey,),
              Row(children: <Widget>[
                Icon(Icons.linear_scale,size:30,color: Colors.blue,),
                Icon(Icons.send,size:30,color: Colors.blue,),
              ],),
              Icon(Icons.sim_card,size: 90,color: Colors.blue,),
            ],
          ),SizedBox(height: 30,),
          Text('Change Number',style: TextStyle(fontSize: 25,),),SizedBox(height: 10,),
          Text('+20++++++++++',style: TextStyle(fontSize: 20,),),SizedBox(height: 30,),
          Container(
              width:MediaQuery.of(context).size.width-120,
              child: Column(
                children: <Widget>[
                  Text( 'You can change your Telegram number here. Your account and all Your cloud data-messages, media, contacts, etc,will be moved to the new number. ',textScaleFactor: 1.1,textAlign:TextAlign.center,style: TextStyle(color: Colors.grey[700],)),
                 SizedBox(height: 130,),
                  FlatButton(
                    onPressed: (){},
                    color: Colors.blue,
                    child: Text('Change Number',style: TextStyle(color: Colors.white,),
                  ),
                  ),
                ],
              ),
          ),
        ],
      )),
    );
  }
}
