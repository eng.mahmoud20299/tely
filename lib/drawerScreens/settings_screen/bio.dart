import 'package:flutter/material.dart';

import '../settings.dart';

class BioScreen extends StatefulWidget {
  @override
  _BioScreenState createState() => _BioScreenState();
}

class _BioScreenState extends State<BioScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text('Bio'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: IconButton(icon: Icon(Icons.done,color:Colors.white),onPressed: (){ Navigator.pop(context);},),
          ),
        ],
      ),
      body: Container(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 30,
                child: TextFormField(
                  maxLength: 70,
                  decoration: InputDecoration(
                    hintText: 'Bio',hintStyle:TextStyle(fontSize: 20),
                  ),
                ),
              ),
              SizedBox(height: 15,),
              Text( 'You can add a few lines about yourself. Anyone who opens your profile will see this text. ',textScaleFactor: 1.28,style: TextStyle(color: Colors.grey[700],)),
            ],
          ),
        ),
      ),
    );
  }
}
