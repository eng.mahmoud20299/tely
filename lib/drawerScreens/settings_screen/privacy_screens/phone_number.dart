import 'package:flutter/material.dart';

import '../privacy.dart';
class PhoneNumberScreen extends StatefulWidget {
  @override
  _PhoneNumberScreenState createState() => _PhoneNumberScreenState();
}

class _PhoneNumberScreenState extends State<PhoneNumberScreen> {
  int selectedOne=0;
  Widget _buildText({String text}) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 20, top: 10,),
          child: Text(text, style: TextStyle(color: Colors.blue, fontSize: 17,),
          ),
        )
      ],
    );
  }
  Widget _buildRow({String text2,int index,bool isSelected}) {
    return InkWell(
      onTap: (){
       setState(() {
         isSelected  =true;
         selectedOne = index;
       });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            child: Text(
              text2,
              style: TextStyle(
                color: Colors.black,
                fontSize: 17,
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child:
              Radio(
                  value: index,
                  groupValue: selectedOne,
                  onChanged: (s){
                    setState(() {
                      isSelected  =true;
                      selectedOne = index;
                    });
                  })
          ),
        ],
      ),
    );
  }
  Widget _buildLine() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
      ),
      height: 1,
      decoration: BoxDecoration(color: Colors.black12),
    );
  }
  Widget _text({String title,Color color,FontWeight weight,double size}){
    return  Text(title,style: TextStyle(color: color,fontWeight: weight,fontSize: size),);
  }
  Widget _row2({String text}){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
          child: Text(
            text,
            style: TextStyle(
              color: Colors.black,
              fontSize: 17,
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            child:Text(
              'Add Users',
              style: TextStyle(
                color: Colors.blue,
                fontSize: 17,
              ),
            )
        ),
      ],
    );
  }
  Widget _container({String text,double height}){
    return  Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.only(left:35,top: 10),
        child: Text(text,textScaleFactor: 1.2,style: TextStyle(color: Colors.grey),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text(
          'Phone Number',
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text:'Who can see my Phone number?'),
          ) ,
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 165,
              child: Column(
                children: <Widget>[
                  _buildRow(text2: 'Everybody', index: 0,isSelected: true),
                  _buildLine(),
                  _buildRow(text2: 'My contacts',index: 1, isSelected: false),
                  _buildLine(),
                  _buildRow(text2: 'Nobody',index: 2,isSelected: false),
                ],
              ),
            ),
          ),
          _container(text: 'Users who have your number saved in their contacts will also see it on Telegram.',height: 70),
         SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text:'Add exceptions'),
          ) ,
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 110,
              child: Column(
                children: <Widget>[
                  _row2(text: 'Always Allow'),
                  _buildLine(),
                  _row2(text: 'Never Allow'),
                ],
              ),
            ),
          ),
          _container(text:'You can add users or entire groups as exceptions that will override the settings above.',height: MediaQuery.of(context).size.height/3+5),
        ],
      ),
    );
  }
}
