import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings_screen/privacy.dart';
class TowStepVerificationScreen extends StatefulWidget {
  @override
  _TowStepVerificationScreenState createState() => _TowStepVerificationScreenState();
}

class _TowStepVerificationScreenState extends State<TowStepVerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only( top: 40,right: 340),
                child: Container(
                    child: InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Colors.grey,
                      ),
                    )),
              ),
              SizedBox(height: 100,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.lock,size: 100,color: Colors.green[500],),
                ],
              ),SizedBox(height: 30,),
              Text('Two-Step Verification',style: TextStyle(fontSize: 25,),),SizedBox(height: 10,),
              Container(
                width:MediaQuery.of(context).size.width-120,
                child: Column(
                  children: <Widget>[
                    Text( 'You can set a password that will be required when you log in on a new device in addition to the code you get in the SMS. ',textScaleFactor: 1.1,textAlign:TextAlign.center,style: TextStyle(color: Colors.grey[700],)),
                    SizedBox(height: 130,),
                    FlatButton(
                      onPressed: (){},
                      color: Colors.blue,
                      child: Text('Set Password',style: TextStyle(color: Colors.white,),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}