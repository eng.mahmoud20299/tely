import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings_screen/privacy.dart';
class LoggedInwithTelegramScreen extends StatefulWidget {
  @override
  _LoggedInwithTelegramScreenState createState() => _LoggedInwithTelegramScreenState();
}

class _LoggedInwithTelegramScreenState extends State<LoggedInwithTelegramScreen> {
  Widget _iconInContainer({IconData icon}){
    return  Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        width: 50,
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(icon,color: Colors.grey,size: 30,),
          ],
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: Colors.grey,width: 3),
        ),
      ),
    );
  }
  Widget _text({String text,FontWeight weight}){
    return Text(text,textScaleFactor: 1.1,style: TextStyle(color: Colors.grey,fontWeight: weight),);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text(
          'Logged in with Telegram',
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _iconInContainer(icon: Icons.star,),
                _iconInContainer(icon: Icons.favorite,),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _iconInContainer(icon: Icons.devices_other,),
                _iconInContainer(icon: Icons.location_on,),
              ],
            ),SizedBox(height: 20,),
            _text(text:'No active logins.',weight: FontWeight.bold ),
            SizedBox(height: 20,),
            _text(text:'You can log in on website that support ' ),
            _text(text:'signing in with Telegram.' ),
          ],
        ),
      )
    );
  }
}