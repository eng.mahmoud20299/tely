import 'package:flutter/material.dart';
import 'package:tely/drawerScreens/settings_screen/privacy.dart';

class BlockedUserScreen extends StatefulWidget {
  @override
  _BlockedUserScreenState createState() => _BlockedUserScreenState();
}

class _BlockedUserScreenState extends State<BlockedUserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text(
          'Blocked User',
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            color: Colors.white,
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left:15,right: 25),
                  child: Icon(Icons.person_add,color: Colors.blue,),
                ),
                Text('Block user',style: TextStyle(color: Colors.blue,fontSize: 18),)
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15),
            child: Text('Blocked user will not be able to contact you and will not see your Last Seen time.',textScaleFactor: 1.2,style: TextStyle(color: Colors.grey[600],) ,),
          )
        ],
      ),
    );
  }
}
