import 'package:flutter/material.dart';

import '../privacy.dart';
class CallsScreen extends StatefulWidget {
  @override
  _CallsScreenState createState() => _CallsScreenState();
}

class _CallsScreenState extends State<CallsScreen> {
  int selectedOne=0;
  Widget _buildText({String text}) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 20, top: 10,),
          child: Text(text, style: TextStyle(color: Colors.blue, fontSize: 17,),
          ),
        )
      ],
    );
  }
  Widget _buildRow({String text2,int index,bool isSelected}) {
    return InkWell(
      onTap: (){
        setState(() {
          isSelected  =true;
          selectedOne = index;
        });
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            child: Text(
              text2,
              style: TextStyle(
                color: Colors.black,
                fontSize: 17,
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child:
              Radio(
                  value: index,
                  groupValue: selectedOne,
                  onChanged: (s){
                    setState(() {
                      isSelected  =true;
                      selectedOne = index;
                    });
                  })
          ),
        ],
      ),
    );
  }
  Widget _buildLine() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
      ),
      height: 1,
      decoration: BoxDecoration(color: Colors.black12),
    );
  }
  Widget _text({String title,Color color,FontWeight weight,double size}){
    return  Text(title,style: TextStyle(color: color,fontWeight: weight,fontSize: size),);
  }
  Widget _row2({String text1,String text2}){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
          child: Text(
            text1,
            style: TextStyle(
              color: Colors.black,
              fontSize: 17,
            ),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 17),
            child:Text(
              text2,
              style: TextStyle(
                color: Colors.blue,
                fontSize: 17,
              ),
            )
        ),
      ],
    );
  }
  Widget _container({String text,double height}){
    return  Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.only(left:35,top: 10),
        child: Text(text,textScaleFactor: 1.2,style: TextStyle(color: Colors.grey),
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text(
          'Calls',
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text:'Who can call me?'),
          ) ,
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 165,
              child: Column(
                children: <Widget>[
                  _buildRow(text2: 'Everybody', index: 0,isSelected: true),
                  _buildLine(),
                  _buildRow(text2: 'My contacts',index: 1, isSelected: false),
                  _buildLine(),
                  _buildRow(text2: 'Nobody',index: 2,isSelected: false),
                ],
              ),
            ),
          ),
          _container(text: "You can restrict who can call you.",height: 40),
          SizedBox(height: 20,),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text:'Add exceptions'),
          ) ,
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 60,
              child: Column(
                children: <Widget>[
                  _row2(text1: 'Never Allow',text2: 'Add Users'),
                ],
              ),
            ),
          ),
          _container(text: "These users will or will not be able to call you regardless of the settings above ",height: 60),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text:'Peer-to-peer'),
          ) ,
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 60,
              child: Column(
                children: <Widget>[
                  _row2(text1: 'Never Allow',text2: 'My Contacts'),
                ],
              ),
            ),
          ),
         Container(
           height: MediaQuery.of(context).size.height/4,
           color: Colors.grey[300],
         )
        ],
      ),
    );
  }
}
