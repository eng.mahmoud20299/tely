import 'package:flutter/material.dart';
import '../privacy.dart';

class ActiveSessionsScreen extends StatefulWidget {
  @override
  _ActiveSessionsScreenState createState() => _ActiveSessionsScreenState();
}

class _ActiveSessionsScreenState extends State<ActiveSessionsScreen> {
  int selectedOne = 0;
  Widget _buildText({String text}) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            left: 20,
            top: 10,
          ),
          child: Text(
            text,
            style: TextStyle(
              color: Colors.blue,
              fontSize: 17,
            ),
          ),
        )
      ],
    );
  }

  Widget _buildLine() {
    return Container(
      margin: EdgeInsets.only(
        left: 15,
      ),
      height: 1,
      decoration: BoxDecoration(color: Colors.black12),
    );
  }

  Widget _text({String title, Color color, FontWeight weight, double size}) {
    return Padding(
      padding: const EdgeInsets.only(right: 120),
      child: Text(
        title,
        style: TextStyle(color: color, fontWeight: weight, fontSize: size),
      ),
    );
  }

  Widget _row2({String text1, String text2, FontWeight weight}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
          child: Text(
            text1,
            style: TextStyle(
                color: Colors.black, fontSize: 17, fontWeight: weight),
          ),
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: Text(
              text2,
              style: TextStyle(
                color: Colors.blue,
                fontSize: 17,
              ),
            )),
      ],
    );
  }

  Widget _container({String text, double height}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: height,
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.only(left: 35, top: 10),
        child: Text(
          text,
          textScaleFactor: 1.2,
          style: TextStyle(color: Colors.grey),
        ),
      ),
    );
  }

  Widget _details(
      {double height, String t1, String t2, String t3, String t4, String t5}) {
    return InkWell(
      onTap: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Terminate sessions'),
                content: Text(
                    'Are you sure you want to terminate all other sessions? '),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      'CANCEL',
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ),
                  FlatButton(
                    child:
                        Text('TERMINATE', style: TextStyle(color: Colors.red)),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ),
                ],
              );
            });
      },
      child: Container(
          height: height,
          child: Column(children: <Widget>[
            Column(
              children: <Widget>[
                _row2(text1: t1, weight: FontWeight.bold, text2: t2),
                _text(
                  title: t3,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 55),
                  child: Column(
                    children: <Widget>[
                      _text(title: t4, color: Colors.grey),
                    ],
                  ),
                ),
              ],
            )
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back)),
        backgroundColor: Color(0xff507ca1),
        title: Text(
          'Devices',
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: _buildText(text: 'Current session'),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Container(
              height: 120,
              child: Column(
                children: <Widget>[
                  _details(
                    height: 65,
                    t1: 'Telegram Android 7.0.1',
                    t2: 'online',
                    t3: 'Realme realme 3pro Android Q (29) ',
                    t4: '41.232.8.252 - Cairo,Egypt ',
                  ),
                  Divider(
                    height: 20,
                    indent: 25,
                    thickness: 1,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 50),
                    child: InkWell(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Text('Terminate sessions'),
                                  content: Text(
                                      'Are you sure you want to terminate all other sessions? '),
                                  actions: <Widget>[
                                    FlatButton(
                                      child: Text(
                                        'CANCEL',
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      color: Colors.white,
                                    ),
                                    FlatButton(
                                      child: Text('TERMINATE',
                                          style: TextStyle(color: Colors.red)),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      color: Colors.white,
                                    ),
                                  ],
                                );
                              });
                        },
                        child: _text(
                            title: 'Terminate All Other Sessions',
                            color: Colors.red)),
                  )
                ],
              ),
            ),
          ),
          _container(text: "Logs out devices except for this one", height: 40),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Column(
              children: <Widget>[
                _buildText(text: 'Active sessions'),
                _buildText(text: 'Scan QR Code'),
                Divider(
                  height: 20,
                  indent: 20,
                  thickness: 1,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Column(

              children: <Widget>[
                _details(
                  height: 65,
                  t1: 'Telegram Android 7.0.1',
                  t2: '12:43 PM',
                  t3: 'Realme realme 3pro Android Q (29) ',
                  t4: '41.232.8.252 - Cairo,Egypt ',
                ),
                Divider(
                  height: 20,
                  indent: 25,
                  thickness: 1,
                ),
                _details(
                  height: 68,
                  t1: 'Telegram Android 7.0.1',
                  t2: 'May 03',
                  t3: 'Realme realme 3pro Android Q (29) ',
                  t4: '41.232.8.252 - Cairo,Egypt ',
                ),
              ],
            ),
          ),
          _container(
              text: 'Tap on a session to terminate.',
              height: MediaQuery.of(context).size.height / 3 ),
        ],
      ),
    );
  }
}
