import 'package:flutter/material.dart';

import '../../settings.dart';
import '../privacy.dart';
import 'pin.dart';
class PasscodeLookScreen extends StatefulWidget {
  @override
  _PasscodeLookScreenState createState() => _PasscodeLookScreenState();
}

class _PasscodeLookScreenState extends State<PasscodeLookScreen> {
  bool value = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[300],
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text('Passcode Look'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: 60,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 15,top: 5),
              child: Column(
                children: <Widget>[
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) =>PinScreen()));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text('Passcode Lock'),
                        Switch(
                          value: value,
                          onChanged: (newValue){
                           setState(() {
                             value = newValue;
                           });
                          },
                        ),
                      ],
                    ),
                  ),
                 Divider(height: 4,),
                ],
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            color: Colors.white,
            child:Padding(
              padding: const EdgeInsets.only(left: 15,top: 10),
              child: Text('Change Passcode',style: TextStyle(color: Colors.grey),),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15,top: 10),
            child: Column(
              children: <Widget>[
                Text('When set up an additional passcode,a lock icon will appear on the chats page.Tap it to lock and unlock your Telegram app. ',textScaleFactor: 1.3,style: TextStyle(color: Colors.grey) ),
                SizedBox(height: 10,),
                Text("Note: if you forget the passcode, you'll need to delete and reinstall the app. All secret chats will be lost.",textScaleFactor: 1.3,style: TextStyle(color: Colors.grey) ),
              ],
            ),
          )
        ],
      ),

    );
  }
}

