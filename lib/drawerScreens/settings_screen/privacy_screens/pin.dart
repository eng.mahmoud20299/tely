import 'package:flutter/material.dart';

import 'passcode_look.dart';
class PinScreen extends StatefulWidget {
  @override
  _PinScreenState createState() => _PinScreenState();
}
class _PinScreenState extends State<PinScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff507ca1),
        leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back,color: Colors.white,)),
        title: InkWell(
          onTap: (){
            showDialog(context: context,
                builder: (BuildContext context){
                  return AlertDialog(
                    title: Container(
                      width: 60,
                      height: 50,
                      child: Column(children: <Widget>[
                        Text('Pin'),
                        Text('Password'),
                      ],),
                    ),
                  );
                }
            );
          },
          child: Container(
            child: Row(
              children: <Widget>[
                Text('Pin'),
                Icon(Icons.arrow_drop_down,)
              ],
            ),
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 15),
            child: Icon(Icons.done,color:Colors.white),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 40),
        child: Center(child: Column(
          children: <Widget>[
            Text('Enter a passcode',style: TextStyle(color: Colors.grey,fontSize: 20),),
            Container(
              width: MediaQuery.of(context).size.width-100,
              child: TextFormField(
                decoration: InputDecoration(
                ),
              ),
            ),

          ],
        ),),
      )
    );
  }
}
